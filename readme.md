# Assignment

A NodeJS server that handles the api's of the application.

# Running the app.

Clone Notifications on your machine and follow the steps:
git clone https://pankaj_shimpi@bitbucket.org/pankaj_shimpi/assignment-instarem.git

Make sure you have node installed on your machine. If not follow the steps:

```
    Follow the download and installation steps from:

    https://nodejs.org/en/download/
```

Make sure you have npm installed on your machine. If not follow the steps:

```
    npm install -g npm@5.7.1
    npm install -g npm@latest
```

Make sure you have MongoDB installed on your machine.

```
    If not please install it by:

        Steps:
        1. sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

        2.
            Ubuntu 12.04
                echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

            Ubuntu 14.04
                echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

            Ubuntu 16.04
                echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

        3. sudo apt-get update

        4. sudo apt-get install -y mongodb-org

        5. Start mongodb service:
            sudo service mongod start


    Or you can follow the download and installation steps from:
        https://docs.mongodb.com/manual/administration/install-community/

    Make sure MongoDB is running on default port 27017.

    Please change config settings in:

        src/config/config.ts

        add all needed details. e.g database name, port, host, etc.
```

Install the server dependencies

```
    npm install
```

Run server

```
    npm run start
```

Import json file from postman folder to the postman .

For every API request we need to generate token. just call api 'http://localhost:8787/getToken' to get the token. Add this token in authorization part of every request. The token will be expired in an hour.

The code is also deployed on heroku. to check the features just replace 'http://localhost:8787' to 'https://assignment-insta-rem.herokuapp.com', and also database is hosted from mlab.
