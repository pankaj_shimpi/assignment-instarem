import app from './App';
const port = process.env.PORT || 8787;
process.on('uncaughtException', function (err) {
  console.log(err);
})
app.listen(port, err => {
  if (err) {
    return console.log(err);
  }

  return console.log(`server is listening on ${port}`);
});
