import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import config from '../config/config';
import { BattleSchema } from '../models/battleSchema';
import { Request, Response } from 'express';

const Battle = mongoose.model('Battle', BattleSchema);

export class BattleController {
  jwtSecret: string = process.env.JWT_SECRET || config.jwt.jwtSecret;
  jwtSessionTimeOut: string =
    process.env.JWT_TIMEOUT || config.jwt.jwtSessionTimeOut;

  public getBattlesCount = (req: Request, res: Response) => {
    Battle.count({}, (err, count) => {
      if (err) {
        res.send(err);
      }
      res.json({ totalCount: count });
    });
  };
  public searchBattles = (req: Request, res: Response) => {
    const params = {
      king: req.query.king || null,
      location: req.query.location || null,
      type: req.query.type || null
    };
    const query = this.generateQuery(params);
    Battle.find(query, (err, battles) => {
      if (err) {
        res.send(err);
      }
      res.json(battles);
    });
  };
  public getBattles = (req: Request, res: Response) => {
    Battle.find({}, (err, battles) => {
      if (err) {
        res.send(err);
      }
      res.json(battles);
    });
  };

  public generateToken = (req: Request, res: Response): any => {
    const token = jwt.sign({}, this.jwtSecret, {
      // this is default algorithm
      algorithm: 'HS256',
      expiresIn: this.jwtSessionTimeOut,
      issuer: 'insta_rem'
    });
    if (!token) {
      res.send({ error: 'Token creation failed!' });
    }
    res.json({ token: token });
  };

  private generateQuery(params) {
    let _query = null;
    let subQuery = [];
    if (params) {
      if (params.king) {
        subQuery.push({
          $or: [{ attacker_king: params.king }, { defender_king: params.king }]
        });
      }
      if (params.location) {
        subQuery.push({
          location: params.location
        });
      }
      if (params.type) {
        subQuery.push({
          battle_type: params.type
        });
      }
      _query = { $and: subQuery };
    }
    return _query;
  }
}
