"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const config_1 = require("../config/config");
const battleSchema_1 = require("../models/battleSchema");
const Battle = mongoose.model('Battle', battleSchema_1.BattleSchema);
class BattleController {
    constructor() {
        this.jwtSecret = process.env.JWT_SECRET || config_1.default.jwt.jwtSecret;
        this.jwtSessionTimeOut = process.env.JWT_TIMEOUT || config_1.default.jwt.jwtSessionTimeOut;
        this.getBattlesCount = (req, res) => {
            Battle.count({}, (err, count) => {
                if (err) {
                    res.send(err);
                }
                res.json({ totalCount: count });
            });
        };
        this.searchBattles = (req, res) => {
            const params = {
                king: req.query.king || null,
                location: req.query.location || null,
                type: req.query.type || null
            };
            const query = this.generateQuery(params);
            Battle.find(query, (err, battles) => {
                if (err) {
                    res.send(err);
                }
                res.json(battles);
            });
        };
        this.getBattles = (req, res) => {
            Battle.find({}, (err, battles) => {
                if (err) {
                    res.send(err);
                }
                res.json(battles);
            });
        };
        this.generateToken = (req, res) => {
            const token = jwt.sign({}, this.jwtSecret, {
                // this is default algorithm
                algorithm: 'HS256',
                expiresIn: this.jwtSessionTimeOut,
                issuer: 'insta_rem'
            });
            if (!token) {
                res.send({ error: 'Token creation failed!' });
            }
            res.json({ token: token });
        };
    }
    generateQuery(params) {
        let _query = null;
        let subQuery = [];
        if (params) {
            if (params.king) {
                subQuery.push({
                    $or: [{ attacker_king: params.king }, { defender_king: params.king }]
                });
            }
            if (params.location) {
                subQuery.push({
                    location: params.location
                });
            }
            if (params.type) {
                subQuery.push({
                    battle_type: params.type
                });
            }
            _query = { $and: subQuery };
        }
        return _query;
    }
}
exports.BattleController = BattleController;
