"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
const expressJWT = require("express-jwt");
const mongoose = require("mongoose");
const config_1 = require("./config/config");
const battleController_1 = require("./controllers/battleController");
var http = require("http");
class App {
    constructor() {
        this.jwtSecret = process.env.JWT_SECRET || config_1.default.jwt.jwtSecret;
        this.mongoUrl = process.env.MONGO_URI || this.generateUrl();
        this.battleController = new battleController_1.BattleController();
        this.addMiddlewares = () => {
            this.express.use(bodyParser.urlencoded({ extended: true }));
            this.express.use(function (req, res, next) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', '*');
                res.header('Origin, X-Requested-With, Content-Type, Accept');
                next();
            });
            this.express.use(bodyParser.json());
            this.express.use(expressJWT({ secret: this.jwtSecret }).unless({
                path: ['/getToken']
            }));
        };
        this.routes = () => {
            //To keep my app up and running
            setInterval(() => {
                http.get("https://assignment-insta-rem.herokuapp.com");
            }, 900000);
            this.express.route('/getToken').post(this.battleController.generateToken);
            this.express.route('/list').get(this.battleController.getBattles);
            this.express.route('/count').get(this.battleController.getBattlesCount);
            this.express.route('/search').get(this.battleController.searchBattles);
        };
        this.mongoSetup = () => {
            mongoose.Promise = global.Promise;
            mongoose.connect(this.mongoUrl);
        };
        this.express = express();
        this.addMiddlewares();
        this.routes();
        this.mongoSetup();
    }
    generateUrl() {
        return `${config_1.default.uri}/${config_1.default.database}`;
    }
}
exports.default = new App().express;
