import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as expressJWT from 'express-jwt';
import * as mongoose from 'mongoose';

import config from './config/config';
import { BattleController } from './controllers/battleController';

var http = require("http");
class App {
  public express;
  jwtSecret = process.env.JWT_SECRET || config.jwt.jwtSecret;
  mongoUrl: string = process.env.MONGO_URI || this.generateUrl();
  battleController: BattleController = new BattleController();
  constructor() {
    this.express = express();
    this.addMiddlewares();
    this.routes();
    this.mongoSetup();
  }

  private addMiddlewares = (): void => {
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.header('Origin, X-Requested-With, Content-Type, Accept');
      next();
    });
    this.express.use(bodyParser.json());
    this.express.use(
      expressJWT({ secret: this.jwtSecret }).unless({
        path: ['/getToken']
      })
    );
  };

  private routes = (): void => {
    //To keep my app up and running
    setInterval(() => {
      http.get("https://assignment-insta-rem.herokuapp.com");
    }, 900000);
    this.express.route('/getToken').post(this.battleController.generateToken);
    this.express.route('/list').get(this.battleController.getBattles);
    this.express.route('/count').get(this.battleController.getBattlesCount);
    this.express.route('/search').get(this.battleController.searchBattles);
  };

  private mongoSetup = (): void => {
    mongoose.Promise = global.Promise;
    mongoose.connect(this.mongoUrl);
  };

  private generateUrl(): string {
    return `${config.uri}/${config.database}`;
  }
}
export default new App().express;
