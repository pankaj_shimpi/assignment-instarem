const config = {
  database: 'battle_db',
  uri: 'mongodb://localhost:27017',
  jwt: {
    jwtSecret: 'jwtSecret',
    jwtSessionTimeOut: '60m'
  }
};
export default config;
