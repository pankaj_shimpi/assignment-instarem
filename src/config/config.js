"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = {
    database: 'battle_db',
    uri: 'mongodb://localhost:27017',
    jwt: {
        jwtSecret: 'jwtSecret',
        jwtSessionTimeOut: '60m'
    }
};
exports.default = config;
